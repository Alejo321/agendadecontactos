<div class="card">
  <div class="card-body">
    <div class="table-responsive">
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th></th>
                <th></th>
                <th>
                    <span class="btn btn-warnig btn-sm">
                        <span class="fa-edit"></span>
                    </span>
                </th>
                <th>
                <span class="btn btn-danger btn-sm">
                    <span class="far fa-trash-alt"></span>
                </span>
                </th> 
            </tr>
        </tbody>
    </table>

    </div>
  </div>
</div>